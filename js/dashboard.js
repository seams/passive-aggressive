window.browser = (function () {
  return window.msBrowser ||
    window.browser ||
    window.chrome;
})();

let App;

(function (a) {
  class Dashboard {
    constructor(filters, target) {
      this._filters = filters;
      this._target = target;
      this._target.innerHTML = "";
      this._data = {};
      this._tags = {};
      this._settings = {};
      this._renderTimeout = -1;
      browser.storage.onChanged.addListener(e => {
        this.getSettings(this.sync.bind(this));
      });
      this.getSettings(this.sync.bind(this));
      browser.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {
          if (request.command === "newData") {
            this.sync();
          }
        }.bind(this)
      );
    }

    getSettings(then) {
      browser.storage.sync.get(s => {
        for(let i in Util.settings) {
          let settingGroup = Util.settings[i];
          for(let o in settingGroup) {
            let setting = settingGroup[o];
            this._settings[setting.name] = s[setting.name] || setting.default;
          }
        }
        then();
      })
    }

    sync() {
      browser.tabs.query({ active: true, currentWindow: true }).then(tabs => {
        browser.tabs.sendMessage(tabs[0].id, { command: "getData" }).then(res => {
          if (res) {
            let tags = {};
            let sortFuncMap = {
              "url": Util.sortUrls
            };
            let clusterFuncMap = {
              "url": Util.getDataHost,
              "custom": e => e.value
            };
            for (let idx in res) {
              for(let e in res[idx].data) {
                let d = res[idx].data[e];
                if(d.tags.length > 0) {
                  for(let tagIdx in d.tags) {
                    let tag = d.tags[tagIdx];
                    if(!tags[tag]) {
                      tags[tag] = 1;
                    }
                    else {
                      tags[tag]++;
                    }
                  }
                }
                else {
                  if(!tags["other"]) {
                    tags["other"] = 1;
                  }
                  else {
                    tags["other"]++;
                  }
                }
              }

              let sortFunc = sortFuncMap[res[idx].type];
              if (sortFunc !== "" && sortFunc !== undefined) {
                res[idx].data.sort(sortFunc.bind(Util));
              }
              else {
                res[idx].data.sort();
              }
              let clusterFunc = clusterFuncMap[res[idx].type];
              if (clusterFunc !== "" && clusterFunc !== undefined) {
                res[idx].data = Util.clusterArray(res[idx].data, clusterFunc);
              }
              else {
                res[idx].data = Util.clusterArray(res[idx].data, e => e.value);
              }
            }
            this._data = res;
            this._tags = tags;

            let debounce = Number(this._settings["renderDebounce"]);
            let action = () => {
              this._renderTimeout = -1;
              this.render();
            }
            if(this._renderTimeout === -1) {
              action();
            }
            else {
              clearTimeout(this._renderTimeout);
              this._renderTimeout = setTimeout(action, debounce);
            }
          }
        }, (e) => {
          this._target.innerText = "This doesn't seem to be a webpage... 1";
        });
      }, () => {
        this._target.innerText = "This doesn't seem to be a webpage... 2";
      });
    }

    getFilterStatus() {
      let out = {};
      let cbs = [...this._filters.querySelectorAll("input")];
      for(let i in cbs) {
        let cb = cbs[i];
        out[cb.id] = cb.checked;
      }
      return out;
    }

    setAllFiltersStatus(val) {
      let status = this.getFilterStatus();
      for(let i in status) {
        let e = document.getElementById(i);
        e.checked = val;
      }
    }

    renderFilters() {
      let filters = this.getFilterStatus();
      this._filters.innerText = "";
      let labels = [];
      let tags = this._tags;
      let other;
      if(this._tags["other"]) {
        other = tags["other"];
        delete tags["other"];
      }
      for (let tag in this._tags) {
        let count = this._tags[tag];
        let filter = this.createFilter(tag, count);
        labels.push(filter.label);
        if(filter.cb.id in filters) {
          filter.cb.checked = filters[filter.cb.id];
        }
        this._filters.appendChild(filter.cb);
        this._filters.appendChild(filter.label);
      }
      if(other) {
        let filter = this.createFilter("other", other);
        if(Object.keys(tags).length === 0) filter.cb.checked = true;
        else filter.cb.checked = false;

        this._filters.appendChild(filter.cb);
        this._filters.appendChild(filter.label);
      }
      Util.randomiseBackgrounds(labels);
    }

    createFilter(tag, count) {
      let id = "toggle-" + Util.attributeFriendly(tag);
      let cb = Util.createElementEx("input", {
        id: id,
        type: "checkbox",
        checked: true
      });
      cb.addEventListener("change", e => this.renderData());

      let label = Util.createElementEx("label", tag + "\u{00A0}(" + count +")");
      if(this._settings["rightClickTags"] === true) {
        label.addEventListener("contextmenu", e => {
          e.preventDefault();
          let status = this.getFilterStatus();
          let checkedCount = 0;
          for(let i in status) {
            if(i === cb.id) continue;
            if(status[i] === true) {
              checkedCount++;
            }
          }
          if(checkedCount === 0) {
            this.setAllFiltersStatus(true);
          }
          else {
            this.setAllFiltersStatus(false);
          }
          cb.checked = true;
          cb.dispatchEvent(new Event("change"));
        });
      }
      label.setAttribute("for", id);
      return {
        label,
        cb
      }
    }

    render() {
      this.renderFilters();
      this.renderData();
    }

    getOpenSections() {
      let sections =  [...this._target.querySelectorAll("details[open]")];
      return sections.map(e => e.querySelector("summary").innerText);
    }

    renderData() {
      let openSections = this.getOpenSections();
      this._target.innerHTML = "";
      let filters = this.getFilterStatus();
      for (let idx in this._data) {
        let sectioncount = 0;
        let target = document.createElement("pre");
        let data = this._data[idx].data;
        for (let i in data) {
          if (typeof data[i] === "object") {
            let list = document.createElement("ul");
            let count = 0;
            data[i].forEach(e => {
              let tags = e.tags;
              if(tags.length === 0) {
                tags = ["other"];
              }
              let include = tags.find(t =>{
                let id = "toggle-" + Util.attributeFriendly(t);
                return filters[id];
              });
              if(include === undefined) return;
              let val = e.value;
              let item = document.createElement("li");
              if(val !== i) {
                val = val.replace(i, "[…]");
              }
              let detailsObject = Util.createDetailsObject(val);
              if(openSections.indexOf(val) !== -1) {
                detailsObject.setAttribute("open", "");
              }
              let itemDetails = Util.addChildren(detailsObject,
                ...Util.renderFinding(e),
                Util.addChildren(Util.createDetailsObject("Raw Info"),
                  Util.objectToTable(e)
                )
              );
              count++;
              item.appendChild(itemDetails);
              list.appendChild(item);
            });
            if(count > 0) {
              let title = i + " (" + count + ")";
              let details = Util.createDetailsObject(title);
              if(openSections.indexOf(title) !== -1) {
                details.setAttribute("open", "");
              }
              details.appendChild(list);
              target.appendChild(details);
              sectioncount++;
            }
          }
        }
        if(sectioncount) {
          let type = this._data[idx].type;
          let title = Util.createElementEx("b", type);
          let section = Util.createDetailsObject(title);
          if(Object.keys(this._data).length === 1 || openSections.indexOf(type) !== -1) {
            section.setAttribute("open", "");
          }
          section.appendChild(target);
          this._target.appendChild(section);
        }
      }

      if (this._target.innerText === "") {
        this._target.innerText = "No data for this tab yet - change filters or refresh the tab.";
      }
    }
  }
  a.Dashboard = Dashboard;
})(App || (App = {}));
