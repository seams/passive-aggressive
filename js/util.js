let Util = {
  settings: {
    "General": [
      {
        name: "fetchConfig",
        label: "Fetch config from the internet",
        element: "input",
        attributes: {
          type: "checkbox"
        },
        valueSelector: "checked",
        default: false
      },
      {
        name: "configUrl",
        label: "URL to fetch config from",
        element: "input",
        attributes: {
          type: "text"
        },
        valueSelector: "value",
        default: "https://gitlab.com/snippets/1924323/raw"
      },
      {
        name: "rightClickTags",
        label: "Right-click a tag to toggle all other tags",
        element: "input",
        attributes: {
          type: "checkbox"
        },
        valueSelector: "checked",
        default: true
      },
      {
        name: "renderDebounce",
        label: "Debounce time between UI redraws",
        element: "input",
        attributes: {
          type: "number"
        },
        valueSelector: "value",
        default: 100
      },
      {
        name: "matchers"
      },
      {
        name: "taggers"
      }
    ],
    "Custom Rules": [
      {
        name: "customMatchers",
        label: "Matchers (JS regex)",
        element: "textarea",
        attributes: {},
        valueSelector: "value",
        default: ""
      }
    ],
  },

  parseUrl: function (url) {
    let sanitisedUrl = url
      .replace("*.", "")
      .replace(":*", "");

    let parser = document.createElement('a');
    parser.href = sanitisedUrl;
    return {
      protocol: parser.protocol,
      host: parser.host,
      hostname: parser.hostname,
      port: parser.port
    };
  },

  isObjectEqual(a, b) {
    return JSON.stringify(a) === JSON.stringify(b);
  },

  getDataHost: function (d) {
    return this.parseUrl(d.value).host;
  },

  clusterArray(a, clusteringFunc) {
    let result = {};
    let extensionId = window.location.host;
    a.forEach(e => {
      let key = clusteringFunc.bind(this)(e);
      if (key === "" || key === undefined || key === extensionId) {
        key = "other";
      }
      if (result[key] === undefined) {
        result[key] = [];
      }
      result[key].push(e);
    });
    return result;
  },

  sortUrls: function (e1, e2) {
    let host1 = this.getDataHost(e1);
    let host2 = this.getDataHost(e2);
    let revhost1 = host1.split(".").reverse().join(".");
    let revhost2 = host2.split(".").reverse().join(".");
    // TODO: write a fancier function that compares the URL parts?
    return revhost1.localeCompare(revhost2);
  },

  createDetailsObject: function (contents) {
    let details = document.createElement("details");
    let summary = document.createElement("summary");
    if(typeof contents === "string") {
      summary.innerText = contents;
    }
    else {
      summary.appendChild(contents);
    }
    details.appendChild(summary);
    return details;
  },

  createTable: function (rows, isFirstHeader) {
    let first = true;
    let table = document.createElement("table");
    rows.forEach(row => {
      let cont = document.createElement("tr");
      table.appendChild(cont);
      row.forEach(e => {
        let d;
        if(first && isFirstHeader) {
          d = document.createElement("th");
        }
        else {
          d = document.createElement("td");
        }
        if (e.constructor.name === "Array") {
          e = JSON.stringify(e);
        }
        if (typeof e === "object") {
          d.appendChild(e);
        }
        else {
          d.innerText = e
        }
        cont.appendChild(d);
      });
      if(first) {
        first = false;
      }
    });
    return table;
  },

  objectToTable: function (obj) {
    let model = [];
    model.push(["Key", "Value"]);
    for (let key in obj) {
      model.push([key, obj[key]]);
    }
    return this.createTable(model, true);
  },

  makeCopyLink: function () {
    let anchor = document.createElement("a");
    anchor.href = "#";
    anchor.innerText = "\u{1f4cb}\u{FE0E}";
    anchor.addEventListener("click", e => {
      e.preventDefault();
      let val = e.target.previousSibling.innerText;
      navigator.clipboard.writeText(val);
    });
    return anchor;
  },

  shortenString: function(str, length) {
    let output = "";
    if(str.length < length) return str;
    const middle = " […] "
    let partLength = (length - middle.length) / 2;
    output += str.substr(0, partLength);
    output += middle;
    output += str.substr(str.length - partLength);
    return output;
  },

  addChildren: function (e, ...args) {
    for (let i in args) {
      e.appendChild(args[i]);
    }
    return e;
  },

  attributeFriendly: function (input) {
    let output = input.toLowerCase().replace(/\s/, "");
    return output;
  },

  randomiseBackgrounds: function (a) {
    let n = a.length;
    for(let i = 0; i < n; i++) {
      let element = a[i];
      element.style.backgroundColor = "hsl(" + (255 * (i/n)) + ", 80%, 60%)"
    }
  },

  createElementEx: function (type, input) {
    let e = document.createElement(type);
    switch(input.constructor.name) {
      case "String":
        e.innerText = input;
        break;
      case "Array":
        this.addChildren(e, ...input);
        break;
      case "Object":
        for(let i in input) {
          e[i] = input[i];
        }
        break;
    }
    return e;
  },

  toggleTag: function(tag) {
    let cb = document.getElementById("toggle-" + this.attributeFriendly(tag));
    cb.checked ^= true;
    cb.dispatchEvent(new Event("change"));
  },

  renderFinding: function (finding) {
    let returns = [];
    let data = this.createElementEx("span", finding.value);
    let br = document.createElement("br");
    let tags = [];
    for(let i in finding.tags) {
      let tagName = finding.tags[i];
      let tag = this.createElementEx("a", tagName);
      tag.href = "#";
      tag.addEventListener("click", e => {
        e.preventDefault();
        this.toggleTag(tagName);
      });
      tag.className = "tag";
      tags.push(tag);
    }
    if(tags.length > 0) {
      tags.unshift(this.createElementEx("span", {innerText: "🏷️\uFE0E"}));
    }
    returns.push(data, this.makeCopyLink(), br, ...tags);
    return returns;
  }
}
