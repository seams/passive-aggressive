(function (a) {
  class Settings {
    constructor(target) {
      if(target) {
        this._form = target;
        document.addEventListener("DOMContentLoaded", this.restoreOptions.bind(this));
        this._form.addEventListener("submit", this.saveOptions.bind(this));
        this.render();
      }
    }

    render() {
      let form = this._form;
      let first = true;
      for(let i in Util.settings) {
        let settings = Util.settings[i];
        let data = [];
        let section = Util.createDetailsObject(i);
        if(first) {
          section.setAttribute("open", "");
          first = false;
        }
        settings.forEach(setting => {
          if(setting.element) {
            let label = Util.createElementEx("p", setting.label);
            let element = document.createElement(setting.element);
            for(let attr in setting.attributes) {
              let val = setting.attributes[attr];
              element[attr] = val;
            }
            element.id = setting.name;

            let reset = document.createElement("input");
            reset.type = "button";
            reset.value = "reset";
            reset.addEventListener("click", (e) => {
              e.target.parentNode.previousSibling.firstChild[setting.valueSelector] = setting.default;
            });
            data.push([label, element, reset]);
          }
        });
        section.appendChild(Util.createTable(data));
        form.appendChild(section);
      }
      form.appendChild(document.createElement("br"));
      let submit = document.createElement("button");
      submit.type = "submit";
      submit.innerText = "Save";
      form.appendChild(submit);
      let saveLabel = document.createElement("span");
      form.appendChild(saveLabel);
      saveLabel.className = "fade";
      this.saveLabel = saveLabel;
    }

    saveOptions(e) {
      e.preventDefault();
      this.saveLabel.style.animation = "none";
      this.saveLabel.innerText = "Saved!";
      void this.saveLabel.offsetHeight;
      this.saveLabel.style.animation = "";
      let out = {};

      for(let i in Util.settings) {
        let settings = Util.settings[i];
        settings.forEach(setting => {
          if(setting.element) {
            let val = document.getElementById(setting.name)[setting.valueSelector];
            out[setting.name] = val;
          }
        });
      }
      browser.storage.sync.set(out);
    }

    restoreOptions() {
      browser.storage.sync.get().then(saved => {

        for(let i in Util.settings) {
          let settings = Util.settings[i];
          settings.forEach(setting => {
            if(setting.element) {
              document.getElementById(setting.name)[setting.valueSelector] = saved[setting.name] || setting.default;
            }
          });
        }
      });
    }
  }
  if (a) a.Settings = new Settings(a);
  else new Settings();
})(document.getElementById("settings"))
