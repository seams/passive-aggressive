window.browser = (function () {
  return window.msBrowser ||
    window.browser ||
    window.chrome;
})();

let cachedSettings = [
  "taggers"
];

(function (a) {
  class ClientScript {
    constructor() {
      this._data = [];
      this.taggers = [];
      this.settings = {};
      this.syncSettings();
      browser.runtime.onMessage.addListener(this.eventCoordinator.bind(this));
      browser.storage.onChanged.addListener(this.syncSettings.bind(this));
    }

    syncSettings() {
      console.log("Client script synchronising settings");
      browser.storage.sync.get(cachedSettings).then(saved => {
        for(let i in Util.settings) {
          let settingGroup = Util.settings[i];
          for(let o in settingGroup) {
            let setting = settingGroup[o];
            if(cachedSettings.indexOf(setting.name) !== -1) {
              this.settings[setting.name] = saved[setting.name] || setting.default;
            }
          }
        }
        this.taggers = [];
        if (this.settings["taggers"]) {
          this.taggers = this.settings["taggers"].map(t => {
            return {
              expression: new RegExp(t.expression, "gi"),
              tags: t.tags
            }
          });
        }
      });
    }

    addData(d) {
      let idx = this.getDataIndex(d.type);
      if (idx === -1) {
        this._data.push({
          type: d.type,
          data: []
        });
        this.sortData();
        idx = this.getDataIndex(d.type);
      }

      this.taggers.forEach(t => {
        let res = [...d.value.matchAll(t.expression)];
        res.forEach(r => {
          d.tags.push(...t.tags);
        });
      });

      if(this._data[idx].data.findIndex(e => Util.isObjectEqual(e, d)) === -1) {
        this._data[idx].data.push(d);
        return true;
      }
      return false;
    }

    sortData() {
      this._data.sort((e1, e2) => e1.type < e2.type);
    }

    getDataIndex(type) {
      return this._data.findIndex(d => d.type === type);
    }

    getDataSize() {
      let total = 0;
      this._data.forEach(d => total += d.data.length);
      return total;
    }

    eventCoordinator(request, sender, sendResponse) {
      switch (request.command) {
        case "newFinding":
          if (this.addData(request.data)) {
            browser.runtime.sendMessage({ command: "newData" }, {});
          }
          sendResponse(this.getDataSize());
          break;
        case "getData":
          sendResponse(this._data);
          break;
      }
    }
  }
  if (a) a.ClientScript = new ClientScript();
  else new ClientScript();
})()
