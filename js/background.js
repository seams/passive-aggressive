window.browser = (function () {
  return window.msBrowser ||
    window.browser ||
    window.chrome;
})();

(function (a) {
  let ignoredTypes = [
    "image",
    "font",
    "media"
  ];

  let decoderMap = {
    "content-security-policy": function (v) {
      let ignore = [
        "",
        "data:",
        "blob:",
        "https:",
        ",",
        "'unsafe-inline'",
        "unsafe-inline",
        "'unsafe-eval'",
        "unsafe-eval",
        "*",
        "'none'",
        "none",
        "'self'",
        "self",
        "default-src",
        "script-src",
        "style-src",
        "img-src",
        "connect-src",
        "font-src",
        "object-src",
        "media-src",
        "frame-src",
        "sandbox",
        "report-uri",
        "child-src",
        "worker-src",
        "form-action",
        "frame-ancestors",
        "plugin-types"
      ];
      let d = [];
      let sections = v.trim().split(";");
      for (let i in sections) {
        let section = sections[i].trim();
        let parts = section.split(" ");
        for (let o in parts) {
          let part = parts[o].trim();
          if (!ignore.includes(part.toLowerCase())) {
            d.push(part);
          }
        }
      }
      return d;
    },

    "access-control-allow-origin": function (v) {
      let ignore = [
        "",
        "*",
      ];
      let d = [];
      let part = v.trim();
      if (!ignore.includes(part.toLowerCase())) {
        d.push(part);
      }
      return d;
    }
  };

  let headerTagMap = {
    "x-amz-security-token": "AWS",
    "x-amz-content-sha256": "AWS",
    "x-amz-date": "AWS",
    "x-amz-bucket-region": "AWS",
    "x-amz-cf-id": "AWS",
    "x-amz-cf-pop": "AWS",

    "x-ms-request-id": "Azure",
  };

  let cachedSettings = [
    "customMatchers",
    "configUrl",
    "matchers"
  ];

  class Background {
    constructor() {
      this.settings = {};
      this.syncSettings();

      browser.storage.onChanged.addListener(this.syncSettings.bind(this));

      browser.webRequest.onBeforeRequest.addListener(
        this.checkBody.bind(this),
        { urls: ["*://*/*"] },
        ["blocking"]
      );
      browser.webRequest.onResponseStarted.addListener(
        this.checkHeaders.bind(this),
        { urls: ["*://*/*"] },
        ["responseHeaders"]
      );

      browser.alarms.create("matcherFetcher", {
        periodInMinutes: 1
      });

      browser.alarms.onAlarm.addListener(alarm => {
        if (alarm.name === "matcherFetcher") {
          browser.storage.sync.get(["fetchConfig", "configUrl"]).then(saved => {
            if(saved.fetchConfig === true) {
              console.log("Fetching config from \"" + saved.configUrl + "\"");
              this.getConfig(saved.configUrl);
            }
          });
        }
      });
    }

    getConfig(url) {
      let req = new XMLHttpRequest();
      req.open('GET', url, true);
      req.setRequestHeader("Accept", "application/json");
      req.onload = function() {
        if (this.status >= 200 && this.status < 400) {
          try {
            let data = JSON.parse(this.response);
            browser.storage.sync.set({
              "matchers": data.matchers,
              "taggers": data.taggers
            });
            console.log("")
            console.log(data);
          } catch (error) {
            console.log("Unable to deserialise config");
            console.log(this.response);
          }
        } else {
          // We reached our target server, but it returned an error
          console.log("response error!");
          console.log(arguments);
        }
      };

      req.onerror = function() {
        // There was a connection error of some sort
        console.log("request error!");
        console.log(arguments);
      };

      req.send();
    }

    syncSettings() {
      console.log("Background script synchronising settings");
      browser.storage.sync.get(cachedSettings).then(saved => {
        for(let i in Util.settings) {
          let settingGroup = Util.settings[i];
          for(let o in settingGroup) {
            let setting = settingGroup[o];
            if(cachedSettings.indexOf(setting.name) !== -1) {
              this.settings[setting.name] = saved[setting.name] || setting.default;
            }
          }
        }
        this.customMatchers = [];
        if (this.settings["customMatchers"]) {
          this.customMatchers = this.settings["customMatchers"].split("\n").map(m => {
            return new RegExp(m, "gi");
          });
        }

        this.matchers = [];
        if (this.settings["matchers"]) {
          this.matchers = this.settings["matchers"].map(m => {
            return {
              type: m.type,
              expression: new RegExp(m.expression, "gi"),
              tags: m.tags
            }
          });
        }
      });
    }

    publishFinding(tabId, finding) {
      browser.tabs.sendMessage(tabId, {
        command: "newFinding",
        data: finding
      }).then(response => {
        if (response) {
          browser.browserAction.setBadgeText({
            text: response.toString(),
            tabId: tabId
          });
        }
      }, error => {
        console.log("Error: ", error, "when sending finding", finding);
      });

    }

    checkHeaders(details) {
      if (details.tabId === -1) return;

      let tags = [];
      for (let i in details.responseHeaders) {
        let header = details.responseHeaders[i];
        let name = header.name.toLowerCase();
        let tag = headerTagMap[name];
        if (tag !== null && tag !== undefined) {
          if (tags.indexOf(tag) === -1) {
            tags.push(tag);
          }
        }

        let f = decoderMap[name];
        if (f !== null && f !== undefined) {
          let v = f(header.value);

          for (i in v) {
            this.publishFinding(details.tabId, {
              type: "url",
              origin: name + " response header on " + details.url,
              matcher: "[Response Header]",
              value: v[i],
              tags: []
            });
          }
        }
      }

      this.publishFinding(details.tabId, {
        type: "url",
        origin: "webRequest",
        matcher: "[Queried URL]",
        value: details.url,
        tags: tags
      });
    }

    checkBody(details) {
      if (details.tabId === -1) return;

      if (ignoredTypes.includes(details.type)) return;

      let filter = browser.webRequest.filterResponseData(details.requestId);
      let decoder = new TextDecoder("utf-8");
      let data = [];
      filter.ondata = event => {
        data.push(event.data);
        filter.write(event.data);
      };

      filter.onstop = event => {
        filter.close();
        let str = "";
        for (let i = 0; i < data.length; i++) {
          let stream = (i == data.length - 1) ? false : true;
          str += decoder.decode(data[i], {stream});
        }
        this.customMatchers.forEach(matcher => {
          let res = [...str.matchAll(matcher)];
          res.forEach(r => {
            this.publishFinding(details.tabId, {
              type: "custom",
              origin: "request body of " + details.url + " index " + r.index,
              matcher: matcher.toString(),
              value: r.toString(),
              tags: []
            });
          });
        });

        this.matchers.forEach(matcher => {
          let res = [...str.matchAll(matcher.expression)];
          res.forEach(r => {
            this.publishFinding(details.tabId, {
              type: matcher.type,
              origin: "request body of " + details.url + " index " + r.index,
              matcher: matcher.expression.toString(),
              value: r.toString(),
              tags: matcher.tags
            })
          });
        });
      }
    }
  }
  if (a) a.Background = new Background();
  else new Background();
})()
